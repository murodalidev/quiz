

def subject_path(instance, filename):
    return "subjects/{0}/{1}".format(instance.id, filename)


def question_path(instance, filename):
    return "questions/{0}/{1}".format(instance.id, filename)


def answer_path(instance, filename):
    return "questions/{0}/answers/{1}".format(instance.question.id, filename)