from django.db import models

from apps.utils.models import BaseModel
from .utils import subject_path, question_path, answer_path


class Subject(BaseModel):
    image = models.ImageField(upload_to=subject_path)
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Question(BaseModel):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='questions')
    name = models.CharField(max_length=255)
    title = models.ImageField(upload_to=question_path, null=True, blank=True)

    def __str__(self):
        return self.title


class Option(BaseModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='options')
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to=answer_path, null=True, blank=True)
    is_true = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Quiz(BaseModel):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='quizzes')
    is_completed = models.BooleanField(default=False)
    expired_time = models.DateTimeField()
    quiz = models.JSONField()
    result = models.JSONField(null=True)



"""
QUIZ
{
    user: {
        id: 1,
        ...
    },
    quiz: {
        subjects: [
            {
                id: 1,
                title: "",
                image: "",
                questions: [
                    {
                        title: "",
                        image: "",
                        is_answered: True,
                        options: [
                            {
                                id: 1,
                                title: "",
                                image: "",
                                is_true: False,
                            },
                            ...
                        ]
                    }
                    ...
                ]
            },
            ...
        ]
    }
}
"""


"""
REQUEST
{
    question_id: 1,
    option_id: 1,
}
"""