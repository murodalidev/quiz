from django.urls import path, include

app_name = 'quiz'

urlpatterns = [
    path('v1/', include('apps.quiz.api.v1.url'))
]
