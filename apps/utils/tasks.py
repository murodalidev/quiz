import os
import uuid
import datetime
import base64
import requests
from celery import shared_task

from apps.utils.utils import code_generator
from apps.users.models import UserResetToken


@shared_task
def send_sms(user_id, phone: str) -> object:
    url = os.getenv("SMS_URL")
    username = os.getenv("SMS_USERNAME")
    password = os.getenv("SMS_PASSWORD")

    # content = code_generator()
    content = "1234"

    data = {
        "header": {
            "login": username,
            "pwd": password,
            "CgPN": "ABITURIENT_"
        },
        "body": {
            "message_id_in": str(uuid.uuid4()),
            "CdPN": phone,
            "text": content
        }
    }

    expire_seconds = int(os.environ.get('EXPIRE_SECONDS'))
    expire_date = datetime.datetime.now() + datetime.timedelta(seconds=expire_seconds)

    encoded_content = base64.b64encode(content.encode('ascii')).decode('ascii')
    UserResetToken.objects.create(user_id=user_id, content=encoded_content, expire_date=expire_date)

    # response = requests.post(url, json=data)
    response = True
    return response
