import random
import string

SHORTCODE_MIN = 4


def code_generator(size=SHORTCODE_MIN, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
