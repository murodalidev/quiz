from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from rest_framework_simplejwt.tokens import RefreshToken


class UserManager(BaseUserManager):
    def create_user(self, phone, password=None, **kwargs):
        if phone is None:
            raise TypeError({"success": False, "detail": _("User should have a phone")})
        user = self.model(phone=phone, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone, password=None, **kwargs):
        user = self.create_user(phone=phone, password=password, **kwargs)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.is_verified = True
        user.role = 0
        user.save(using=self._db)
        return user


def avatar_path(instance, filename):
    phone = instance.phone
    return 'users/{0}/{1}'.format(phone, filename)


class User(AbstractBaseUser, PermissionsMixin):
    """
    register
        1. register
        2. send sms (task)
        3. verify
        4. login (tokens)

    login
        - phone
        - password

    change password
        - old password
        - new password
        - new password again

    reset password
        1. send sms
        2. verify
        3. set new password
            - new password
            - new password again
    """

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    phone = models.CharField(max_length=12, verbose_name=_('Phone number'), unique=True, db_index=True)  # 998974441122
    full_name = models.CharField(max_length=50, verbose_name=_('Full name'), null=True)
    birth_date = models.DateField(null=True, blank=True, verbose_name=_('Birth data'))
    avatar = models.ImageField(upload_to=avatar_path, null=True, blank=True)
    is_superuser = models.BooleanField(default=False, verbose_name=_('Super user'))
    is_staff = models.BooleanField(default=False, verbose_name=_('Staff user'))
    is_active = models.BooleanField(default=False, verbose_name=_('Active user'))
    modified_date = models.DateTimeField(auto_now=True, verbose_name=_('Modified date'))
    created_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Created date'))

    objects = UserManager()

    EMAIL_FIELD = ''
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.phone


class UserResetToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(editable=False)
    expire_date = models.DateTimeField(db_index=True)
    is_used = models.BooleanField(default=False)
    modified_date = models.DateTimeField(auto_now=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.phone
