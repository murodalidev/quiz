from django.urls import path

from apps.users.api.v1.views import auth

urlpatterns = [
    # auth api
    path('auth/register/', auth.UserRegisterAPIView.as_view(), name='register'),
    path('auth/logout/', auth.UserLogoutAPIView.as_view(), name='logout'),
    path('auth/login/', auth.UserLoginAPIView.as_view(), name='login'),
    path('auth/verify/', auth.UserVerifyPhoneAPIView.as_view(), name='verify'),
    path('auth/set-password/', auth.UserSetPasswordAPIView.as_view(), name='create-password'),
    path('auth/send-sms/', auth.UserSendSmsPhoneAPIView.as_view(), name='send-sms'),
    path('auth/change-password/', auth.UserChangePasswordAPIView.as_view(), name='change-password'),
]

"""
register:
    - auth/register/ (send_code)
    - auth/verify/ (get tokens)
    - auth/set-password/
login:
    - auth/login/
change password:
    - auth/change-password/
reset password:
    - auth/send-sms/
    - auth/verify/ (get tokens)
    - auth/set-password/
"""
