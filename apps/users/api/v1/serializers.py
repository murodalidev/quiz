import base64

from django.utils import timezone
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from rest_framework.validators import UniqueValidator
from django.utils.translation import gettext_lazy as _

from apps.users.models import User, UserResetToken
from apps.utils.validators import validate_phone_number


class UserRegisterSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(
        required=True, max_length=12,
        validators=[
            UniqueValidator(queryset=User.objects.all()),
            validate_phone_number
        ]
    )

    class Meta:
        model = User
        fields = ['phone', 'full_name']


class UserVerifySerializer(serializers.Serializer):
    phone = serializers.CharField(
        required=True, max_length=12,
        validators=[validate_phone_number]
    )
    code = serializers.CharField(min_length=4, required=True)

    class Meta:
        model = User
        fields = ['phone', 'code']

    def validate(self, attrs):
        phone = attrs.get('phone')
        code = attrs.get('code')

        user = User.objects.filter(phone=phone).first()
        if not user:
            raise ValidationError({'error': _('This phone number is not defined')})

        user_tokens = UserResetToken.objects.filter(user=user.id)
        if not user_tokens:
            raise ValidationError({'error': _('User not found, resend sms code')})

        user_token = user_tokens.last()
        if user_token.is_used:
            raise ValidationError({'error': _('Verification code already used')})
        user_token.is_used = True
        user_token.save()

        if code != base64.b64decode(user_token.content).decode('utf-8'):
            raise ValidationError({'error': _('Verification code did not match')})

        timestamp = timezone.now() > user_token.expire_date
        if timestamp:
            raise ValidationError({'error': _('Verification code is not expired, get new one')})

        return attrs


class UserSendSmsSerializer(serializers.Serializer):
    phone = serializers.CharField(
        required=True, max_length=12,
        validators=[validate_phone_number]
    )

    class Meta:
        model = User
        fields = ['phone']

    def validate(self, attrs):
        phone = attrs['phone']
        user = User.objects.filter(phone=phone).first()
        if not user:
            raise ValidationError({'error': _("User not found")})

        return user


class UserLoginSerializer(serializers.Serializer):
    phone = serializers.CharField(
        required=True, max_length=12,
        validators=[validate_phone_number]
    )
    password = serializers.CharField(min_length=4, required=True)

    class Meta:
        model = User
        fields = ['phone', 'password']

    def validate(self, attrs):
        phone = attrs.get('phone')
        password = attrs.get('password')
        user = authenticate(phone=phone, password=password)
        if not user:
            raise ValidationError({'error': _('User not found')})
        if not user.is_active:
            raise ValidationError({'error': _('User is not active, you should verify first')})
        return attrs


class UserChangePasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(max_length=68, write_only=True)
    password = serializers.CharField(min_length=6, max_length=68, write_only=True)
    password2 = serializers.CharField(min_length=6, max_length=68, write_only=True)

    class Meta:
        model = User
        fields = ['old_password', 'password', 'password2']

    def validate(self, attrs):
        user = self.context['request'].user
        old_password = attrs.get('old_password')
        password = attrs.get('password')
        password2 = attrs.get('password2')
        if user.check_password(old_password):
            if password != password2:
                raise ValidationError({'error': _('passwords did not match')})
            return attrs
        raise ValidationError({'error': _('Old passwords did not match')})


class UserSetPasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(min_length=4, write_only=True)
    password2 = serializers.CharField(min_length=4, write_only=True)

    class Meta:
        model = User
        fields = ['password', 'password2']

    def validate(self, attrs):
        password = attrs.get('password')
        password2 = attrs.get('password2')

        if password != password2:
            raise ValidationError({'error': _('Passwords did not match')})
        return attrs


class UserLogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField(required=True)
