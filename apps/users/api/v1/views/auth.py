from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken

from apps.users.api.v1.serializers import (
    UserRegisterSerializer,
    UserSetPasswordSerializer,
    UserLoginSerializer,
    UserVerifySerializer,
    UserSendSmsSerializer,
    UserChangePasswordSerializer,
    UserLogoutSerializer
)
from apps.utils.tasks import send_sms
from apps.users.models import User


class UserRegisterAPIView(generics.GenericAPIView):
    # http://127.0.0.1:8000/users/v1/auth/register/
    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()
        phone = serializer.validated_data['phone']
        send_sms.delay(obj.id, phone)
        data = {
            "success": True,
            "detail": _("verification code sent your phone"),
        }
        return Response(data, status=status.HTTP_201_CREATED)


class UserVerifyPhoneAPIView(generics.GenericAPIView):
    # http://127.0.0.1:8000/users/v1/auth/user-verify/
    serializer_class = UserVerifySerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone = serializer.validated_data['phone']
        user = User.objects.get(phone__exact=phone)
        user.is_active = True
        user.save()
        refresh = RefreshToken.for_user(user)
        data = {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }
        return Response(data, status=status.HTTP_200_OK)


class UserSendSmsPhoneAPIView(generics.GenericAPIView):
    # http://127.0.0.1:8000/users/v1/auth/send-sms/
    serializer_class = UserSendSmsSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        send_sms.delay(serializer.validated_data.id, serializer.validated_data.phone)
        data = {
            "success": True,
            "detail": _("verification code sent your phone"),
        }
        return Response(data, status=status.HTTP_200_OK)


class UserSetPasswordAPIView(generics.CreateAPIView):
    serializer_class = UserSetPasswordSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data['password']
        user.set_password(password)
        user.save()
        return Response({'success': True}, status=status.HTTP_200_OK)


class UserLoginAPIView(generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserLoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone = serializer.validated_data['phone']
        user = User.objects.get(phone__exact=phone)
        refresh = RefreshToken.for_user(user)
        data = {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }
        return Response(data, status=status.HTTP_200_OK)


class UserChangePasswordAPIView(generics.GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserChangePasswordSerializer
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        user = request.user
        password = request.data.get('password')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user.set_password(password)
        user.save()
        data = {
            "success": True,
            "detail": "Password changed"
        }
        return Response(data, status=status.HTTP_200_OK)


class UserLogoutAPIView(generics.GenericAPIView):
    serializer_class = UserLogoutSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        refresh = serializer.validated_data['refresh']
        try:
            RefreshToken(refresh).blacklist()
            OutstandingToken.objects.filter(user=request.user).delete()
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_205_RESET_CONTENT)
