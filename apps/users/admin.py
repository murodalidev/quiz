from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User, UserResetToken
from .forms import UserCreationForm, UserChangeForm


class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('id', 'phone', 'full_name', 'birth_date', 'is_superuser', 'is_staff', 'is_active', 'created_date')
    readonly_fields = ('last_login', 'modified_date', 'created_date')
    list_filter = ('is_superuser', 'is_staff', 'is_active')
    date_hierarchy = 'created_date'
    ordering = ()
    fieldsets = (
        (None, {'fields': ('phone', 'password', 'full_name', 'birth_date', 'avatar')}),
        (_('Permissions'), {'fields': ('is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'modified_date', 'created_date')}),
    )
    add_fieldsets = (
        ('Creating a new user', {'classes': ('wide',), 'fields': ('phone', 'password1', 'password2'), }),
    )
    search_fields = ('phone', 'full_name')


class UserResetTokenAdmin(admin.ModelAdmin):
    search_fields = ('user__phone', )
    list_display = ('id', 'user', 'expire_date', 'is_used', 'modified_date', 'created_date')
    readonly_fields = ('user', 'content', 'expire_date', 'is_used', 'modified_date', 'created_date')
    date_hierarchy = 'created_date'
    list_filter = ('is_used', )


admin.site.register(User, UserAdmin)
admin.site.register(UserResetToken, UserResetTokenAdmin)
